﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            // создаю парочку видов оружий
            Knife knife = new Knife();
            Pistol pistol = new Pistol();
            //а тут у нас инициализация наших оружий
            knife.WeaponSetting();
            pistol.WeaponSetting();
            //а это мы создаём наших игроков
            Player children = new Player("Петя со двора", 100, pistol);
            Player teenager = new Player("Вася старшеклассник", 100, knife);
            //а тут показваю, что у них интересного есть
            children.ShowInfo();
            teenager.ShowInfo();
            // да начнётся битва!
            Match(teenager, children, knife, pistol);

            static void Match(Player player, Player player1, Weapon weapon, Weapon weapon1)
            {
                //переменные для боёвки
                Random random = new Random();
                int shotplayer;
                int shotplayer1;
                int i = 0;
                do
                {
                    shotplayer = random.Next(4);
                    shotplayer1 = random.Next(2);

                    //удар игрока, первый защищается
                    if (shotplayer1 == 1)
                    {
                        Console.WriteLine($"{player.name} получил по почкам от {player1.name}, который засадил ему из {player1.weapon}");
                        player.health -= weapon1.damage;
                    } else 
                    {
                        Console.WriteLine($"{player.name} уклонился от {player1.name}, который пытался использовать {player1.weapon}");
                    }

                    if (player.health <= 0)
                    {
                        Console.WriteLine($"{player.name} с здоровьем {player.health}--------раунд {i} закончен----------{player1.name} с здоровьем {player1.health}");
                        break;
                    }

                    if (shotplayer == 1)
                    {
                        Console.WriteLine($"{player1.name} получил по лицу от {player.name}, который засадил ему из {player.weapon}");
                        player1.health -= weapon.damage;
                    }
                    else
                    {
                        Console.WriteLine($"{player1.name} уклонился от {player1.name}, который пытался использовать {player.weapon}");
                    }
                    i++;
                    Console.WriteLine($"{player.name} с здоровьем {player.health}--------раунд {i} закончен----------{player1.name} с здоровьем {player1.health}");
                } while (player.health > 0 && player1.health > 0);
                // проверяем кто победил
                if (player.health <= 0)
                {
                    Console.WriteLine($"По итограм {i} раундов победил: {player1.name}!");
                } else
                {
                    Console.WriteLine($"По итограм {i} раундов победил: {player.name}!");
                }


            }


        }
    }

    public class Player
    {
        public string name { get; set; }
        public int health { get; set; }
        public string weapon { get; set; }

        public Player(string name, int health, Weapon weapon)
        {
            this.name = name;
            this.weapon = weapon.name;
            this.health = health;
        }

        public void ShowInfo()
        {
            Console.WriteLine($"У {name}, есть {weapon}, и его здоровье {health}xp");
            Console.WriteLine("----------------");
        }


    }

    public abstract class Weapon
    {  
        public string name { get; set; }
        public int damage { get; set; }
        public int range { get; set; }

        public abstract void WeaponSetting();

        public void WeaponFunction(string name, int damage, int range)
        {
            this.name = name;
            this.damage = damage;
            this.range = range;
            Console.WriteLine($"{name} наносит урона {damage} xp, и лупит на {range} метр дистанции");
        }


    }

    public class Knife : Weapon
    {      
        public override void WeaponSetting()
        {
            Console.WriteLine("этот нож чертовски опасен!");
            WeaponFunction("Кухонный нож", 50, 1);
            Console.WriteLine("----------------");
        }
        
        

    }

    public class Pistol : Weapon
    {
        public override void WeaponSetting()
        {
            Console.WriteLine("этот пистолет очень опасен!");
            WeaponFunction("Водяной пистолет", 20, 10);
            Console.WriteLine("----------------");
        }
    }
}
