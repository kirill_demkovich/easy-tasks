﻿using System;

namespace program1
{
    class Program
    {
      
        static void Main(string[] args)
        { 
            start(); 
        }
        //создаю стартовую функцию
            static void start() { 
            int i;
            do
            {
                Console.WriteLine("Please, choice the task, or press 0 for exit");
                i = Convert.ToInt32(Console.ReadLine());
            }
            while (i < 0 | i > 5);
                switch (i)
                {
                case 0:
                    Environment.Exit(0);
                    break;
                case 1:
                        first();
                        break;
                    case 2:
                    second();
                    break;
                    case 3:
                    third();
                    break;
                    case 4:
                    thourd();
                    break;
                    case 5:
                    five();
                    break;
            }

            }
        // а тут первая задача решается, сумма двух значений
            public static void first()
        {
            Console.WriteLine("sum from two");

            Console.Write("a = ");
            int a = Convert.ToInt32(Console.ReadLine());
            Console.Write("b = ");
            int b = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"a + b = {a + b}");
            start();
        }
        // а тут уже вторая задача решается, площадь прямоугольника
        public static void second()
        {
            Console.WriteLine("square of rectangle");

            Console.Write("First side ");
            int a = Convert.ToInt32(Console.ReadLine());
            Console.Write("Second side ");
            int b = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"The square of rectangle = {a * b} cm2");
            start();
        }

        // а тут третья рассчитаем площадь круга

        public static void third()
        {
            Console.WriteLine("square of circle");
            double pi = Math.PI;
            Console.Write("Radius: ");
            int a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"The square of circle = {Math.Round(a * a * pi, 2)} cm2");
            start();
        }

        // четное или нечетное

        public static void thourd()
        {
            Console.WriteLine("press the number");
            Console.Write("the number: ");
            int a = Convert.ToInt32(Console.ReadLine());
            int b = a % 2;
                if (b == 1)
            {
                   Console.WriteLine($"{a} is odd (nechet)");
            }
                else
            {
                Console.WriteLine($"{a} is even (chet)");
            }
            
            start();
        }

        // считаю сумму всех значений массива
        public static void five()
        {
            Console.WriteLine("sum of the elements massiv");
            Console.WriteLine("press the count of massiv");
            int a = Convert.ToInt32(Console.ReadLine());
            int[] mass = new int[a];
            int sum = 0;
            for (int i = 0; i < a; i++)
            {
                Console.WriteLine($"press the number{i} of massiv");
                mass[i] = Convert.ToInt32(Console.ReadLine());
                sum = sum + mass[i];
            }
            Console.WriteLine($"summ of massiv = {sum}");
            start();
        }
    }
}
