﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    interface IEatable
    {
        void Etable(string name);
    }

    public interface INonEatable
    {
        void NonEtable(string name);
    }

    interface IVegan
    {
        void Vegan(string name);
    }

    interface INonVegan
    {
        void NonVegan(string name);
    }
}
