﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Store
    {
        protected string name;
        protected string adress;
        List<Product> products = new List<Product>();

        public Store(string name, string adress, List<Product> products)
        {
            this.name = name;
            this.adress = adress;
            this.products = products;
        }

        public void ShowStore()
        {
            Console.WriteLine($"Магазин {name} находится по адресу: {adress}");
        }

        public void ShowAllProducts()
        {
            int i = 1;
            ShowStore();
            foreach (var item in products)
            {
                Console.WriteLine($"{i}. {item}");
                i++;
            }
        }

        public void ShowEatableProducts()
        {
            int i = 1;
            ShowStore();
            Console.WriteLine("Вашему вниманию представлены товары, которые можно кушать");
            foreach (var item in products)
            {
                if (item is IEatable)
                {
                    Console.WriteLine($"{i}. {item}");
                    i++;
                }
            }
        }


        public void ShowNonEatableProducts()
        {
            int i = 1;
            ShowStore();
            Console.WriteLine("Вашему вниманию представлены товары, которые НЕЛЬЗЯ кушать");
            foreach (var item in products)
            {
                if (item is INonEatable)
                {
                    Console.WriteLine($"{i}. {item}");
                    i++;
                }
            }
        }

        public void ShowVeganProducts()
        {
            int i = 1;
            ShowStore();
            Console.WriteLine("Вашему вниманию представлены товары, которые кушают веганы");
            foreach (var item in products)
            {
                if (item is IVegan)
                {
                    Console.WriteLine($"{i}. {item}");
                    i++;
                }
            }
        }

        public void ShowNonVeganProducts()
        {
            int i = 1;
            ShowStore();
            Console.WriteLine("Вашему вниманию представлены товары, которые НЕЛЬЗЯ кушать веганам");
            foreach (var item in products)
            {
                if (item is INonVegan)
                {
                    Console.WriteLine($"{i}. {item}");
                    i++;
                }
            }
        }
    }
}
