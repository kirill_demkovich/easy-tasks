﻿using System;


namespace ConsoleApp1
{
    abstract class Product
    {
        protected string name;
        protected float price;

        /*  public Product(string name, float price)
          {
              this.name = name;
              this.price = price;
          }*/
        public override string ToString()
        {
            return "Продукт " + name + " стоимостью " + price + " рублей";
        }

    }



    class Grocery : Product, IEatable
    {
        protected float weight;
        public Grocery(string name, float price, float weight)
        {
            this.name = name;
            this.price = price;
            this.weight = weight;
        }
        public override string ToString()
        {
            return "Продукт " + name + " стоимостью " + price + " рублей и весом " + weight + " килограмм";
        }

        public void Etable(string name)
        {
            Console.WriteLine($"{name} I am etable");
        }
    }

    class Meat : Grocery, INonVegan
    {
        public Meat(string name, float price, float weight) : base(name, price, weight) { }

        public void NonVegan(string name)
        {
            Console.WriteLine($"Привет, я {name} и меня НЕЛЬЗЯ кушать веганам");
        }
    }

    class Fruit : Grocery, IVegan
    {
        public Fruit(string name, float price, float weight) : base(name, price, weight) { }

        public void Vegan(string name)
        {
            Console.WriteLine($"Привет, я {name} и меня кушают веганы");
        }
    }

    class Stationery : Product, INonEatable
    {
        protected int count;
        public Stationery(string name, float price, int count)
        {
            this.name = name;
            this.price = price;
            this.count = count;
        }
        public void NonEtable(string name)
        {
            Console.WriteLine($"{name} I am non etable");
        }

        public override string ToString()
        {
            return "Продукт " + name + " стоимостью " + price + " рублей и в колличестве " + count;
        }
    }
}