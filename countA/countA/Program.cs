﻿using System;
using System.Collections.Generic;

namespace countA
{
    class Program
    {
        static void Main(string[] args)
        {
            RussianToLatin();
            // считаю все положительные числа массива
            static void CountPozitive()
            {
                Console.WriteLine("enter how many numbers do you want to write?");
                int numbers = Convert.ToInt32(Console.ReadLine());
                decimal[] massiv = new decimal[numbers];
                decimal result = 0;
                Console.WriteLine("enter all numbers:");
                for (int i = 0; i < numbers; i++)
                {
                    Console.WriteLine($"{i + 1}: ");
                    massiv[i] = Convert.ToDecimal(Console.ReadLine());
                    if (massiv[i] >= 0)
                    {
                        result += massiv[i];
                    }

                }
                Console.WriteLine($"Summ of pozitive numbers: {result}");
            }

            // считаю только буквы а в введенной строке
            static void CountA()
            {
                Console.WriteLine("enter any sentences:");
                string a = Console.ReadLine();
                int count = 0;
                for (int i = 0; i < a.Length; i++)
                {

                    if (a[i] == 'a' || a[i] == 'A' || a[i] == 'а' || a[i] == 'А')
                    {
                        count++;

                    }

                }
                Console.WriteLine(count);
            }
            //Найти индекс первого элемента массива, который больше введенного числа.
            static void IndexFind()
            {
                Console.WriteLine("enter any numbers:");
                decimal a = Convert.ToDecimal(Console.ReadLine());
                decimal[] mass = { 10, 14, 1, 22, 34.589m, 41.8795m, 145.521m, 8000, 54, 21, 21000, 85, 74, 987888, 10 };
                for (int i = 1; i < mass.Length; i++)
                {
                    if (mass[i] > a)
                    {
                        Console.WriteLine($"{i} - massiv index which more then your number");
                        break;

                    }
                }
            }
            //Подсчет количества “дырок” в цифрах введенного числа.
            static void DirkiFind()
            {
                Console.WriteLine("enter any number:");
                int result = 0;
                string a = Console.ReadLine();
                for (int i = 0; i < a.Length; i++)
                {
                    if (a[i] == '0' || a[i] == '6' || a[i] == '9')
                    {
                        result++;
                    }
                    if (a[i] == '8')
                    {
                        result += 2;
                    }
                }
                Console.WriteLine(result);
            }

            //Подсчет количества гласных букв в введенной строке.
            static void Lettercount()
            {
                Console.WriteLine("enter any sentences:");
                int result = 0;
                string a = Console.ReadLine();
                for (int i = 0; i < a.Length; i++)
                {
                    if (a[i] == 'a' || a[i] == 'e' || a[i] == 'y' || a[i] == 'u' || a[i] == 'i' || a[i] == 'o' ||
                        a[i] == 'я' || a[i] == 'ы' || a[i] == 'а' || a[i] == 'е' || a[i] == 'и' || a[i] == 'о' || a[i] == 'ю' || a[i] == 'э' || a[i] == 'у' ||
                        a[i] == 'A' || a[i] == 'E' || a[i] == 'Y' || a[i] == 'U' || a[i] == 'I' || a[i] == 'O' ||
                        a[i] == 'Я' || a[i] == 'Ы' || a[i] == 'А' || a[i] == 'Е' || a[i] == 'И' || a[i] == 'О' || a[i] == 'Ю' || a[i] == 'Э' || a[i] == 'У')
                    {
                        result++;
                    }
                }
                Console.WriteLine(result);
            }

            //Сортировка массива чисел по возрастанию.(bubble sort).
            static void Bubblesort()
            {
                int[] massiv = { 10, 15, 2, 13, 44, 21, 2, 4, 2, 8 };
                int helpcount;
                for (int i = 1; i < massiv.Length; i++)
                {
                    for (int j = 1; j < massiv.Length; j++)
                    {
                        if (massiv[j] < massiv[j - 1])
                        {
                            helpcount = massiv[j];
                            massiv[j] = massiv[j - 1];
                            massiv[j - 1] = helpcount;
                        }
                    }
                }

                for (int i = 0; i < massiv.Length; i++)
                {
                    Console.Write($"{massiv[i]}; ");
                }
            }

            //Сортировка списка слов в алфавитном порядке.

            static void WordSort()
            {
                List<string> spisok = new List<string>();
                Console.WriteLine("enter how many names do yo want to write?");
                int a = Convert.ToInt32(Console.ReadLine());
                for (int i = 0; i < a; i++)
                {
                    spisok.Add(Console.ReadLine());
                }
                spisok.Sort();
                for (int i = 0; i < spisok.Count; i++)
                {
                    Console.WriteLine(spisok[i]);
                }


            }
            //Из Кириллицы в Латиницу.
            static void RussianToLatin()
            {
                Console.WriteLine("введи слово кириллицей ЗАГЛАВНЫМИ БУКВАМИ");
                string s = Console.ReadLine();
                string ret = "";
                string[] rus = {"А","Б","В","Г","Д","Е","Ё","Ж", "З","И","Й","К","Л","М", "Н",
          "О","П","Р","С","Т","У","Ф","Х", "Ц", "Ч", "Ш", "Щ",   "Ъ", "Ы","Ь",
          "Э","Ю", "Я", " "};
                string[] eng = {"A","B","V","G","D","E","E","ZH","Z","I","Y","K","L","M","N",
          "O","P","R","S","T","U","F","KH","TS","CH","SH","SHCH",null,"Y",null,
          "E","YU","YA", " "};

                for (int j = 0; j < s.Length; j++)
                    for (int i = 0; i < rus.Length; i++)
                        if (s.Substring(j, 1) == rus[i])
                        {
                            ret += eng[i];
                        }
                       
                Console.WriteLine(ret);
            }
            
        }
    }
}

